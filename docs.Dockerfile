FROM debian:buster-slim

RUN apt-get update -y && \
    apt-get install -y \
    doxygen \
    git \
    graphviz \
    make \
    && rm -rf /var/lib/apt/lists/*
